// LCM_NN_N.cpp
// ��������: ���������� ����, ������ 9375

//��� ����������� �����
#pragma once
#include "mul_nn_n.h"
#include "gcf_nn_n.h"

int lcm_nn_n(int a, int b)
{
	int c = -1;
	if (a < 1 || b < 1)
		return c;
	c = mul_nn_n(a, b) / gcf_nn_n(a, b); // �������: ���(a,b)= a*b/���(a,b)
	return c;
}