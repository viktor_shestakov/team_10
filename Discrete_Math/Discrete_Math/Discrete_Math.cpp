﻿#include <iostream>
#include <iomanip>
#include "div_nn_n.h"
#include "div_zz_z.h"
#include "gcf_nn_n.h"
#include "lcm_nn_n.h"
#include "mod_nn_n.h"
#include "add_zz_z.h"
#include "add_nn_n.h"
#include "mul_nd_n.h"
#include "mul_zz_z.h"
#include "sub_zz_z.h"
#include "nzer_n_b.h"
#include "add_1n_n.h"
#include "sub_nn_n.h"
#include "mul_nn_n.h"
#include "sub_ndn_n.h"
#include "mul_nk_n.h"
#include "com_nn_d.h"
#include "mul_zm_z.h"
using namespace std;

int main()
{
	int choice;
	cout << "Wellcome!" << endl << endl;
	cout << "Choose your funcion: " << endl
		<< "1 - COM_NN_D" << setw(42) << "16 - POZ_Z_D (not ready)" << setw(42) << "31 - MUL_QQ_Q (not ready)" << endl
		<< "2 - NZER_N_B" << setw(31) << "17 - MUL_ZM_Z" << setw(53) << "32 - DIV_QQ_Q (not ready)" << endl
		<< "3 - ADD_1N_N" << setw(44) << "18 - TRANS_N_Z (not ready)" << setw(40) << "33 - ADD_PP_P (not ready)" << endl
		<< "4 - ADD_NN_N" << setw(44) << "19 - TRANS_Z_N (not ready)" << setw(40) << "34 - SUB_PP_P (not ready)" << endl
		<< "5 - SUB_NN_N" << setw(31) << "20 - ADD_ZZ_Z" << setw(53) << "35 - MUL_PQ_P (not ready)" << endl
		<< "6 - MUL_ND_N" << setw(31) << "21 - SUB_ZZ_Z" << setw(54) << "36 - MUL_PXK_P (not ready)" << endl
		<< "7 - MUL_NK_N" << setw(31) << "22 - MUL_ZZ_Z" << setw(52) << "37 - LED_P_Q (not ready)" << endl
		<< "8 - MUL_NN_N" << setw(31) << "23 - DIV_ZZ_Z" << setw(52) << "38 - DEG_P_N (not ready)" << endl
		<< "9 - SUB_NDN_N" << setw(42) << "24 - MOD_ZZ_Z (not ready)" << setw(40) << "39 - FAC_P_Q (not ready)" << endl
		<< "10 - DIV_NN_DK (not ready)" << setw(28) << "25 - RED_Q_Q (not ready)" << setw(42) << "40 - MUL_PP_P (not ready)" << endl
		<< "11 - DIV_NN_N " << setw(40) << "26 - INT_Q_B (not ready)" << setw(42) << "41 - DIV_PP_P (not ready)" << endl
		<< "12 - MOD_NN_N" << setw(43) << "27 - TRANS_Z_Q (not ready)" << setw(40) << "42 - MOD_PP_P (not ready)" << endl
		<< "13 - GCF_NN_N" << setw(43) << "28 - TRANS_Q_Z (not ready)" << setw(40) << "43 - GCF_PP_P (not ready)" << endl
		<< "14 - LCM_NN_N" << setw(42) << "29 - ADD_QQ_Q (not ready)" << setw(40) << "44 - DER_P_P (not ready)" << endl
		<< "15 - ABS_Z_N (not ready)" << setw(31) << "30 - SUB_QQ_Q (not ready)" << setw(40) << "45 - NMR_P_P (not ready)" << endl << endl;
	cout << "Your choice: ";
	cin >> choice;
	double a, b, c;
	switch (choice)
	{
	case 11:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (div_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << div_nn_n(a,b);
		break;
	case 23:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << div_zz_z(a, b);
		break;
	case 13:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (gcf_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << gcf_nn_n(a, b);
		break;
	case 14:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (lcm_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << lcm_nn_n(a, b);
		break;
	case 12:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (mod_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mod_nn_n(a, b);
		break;
	case 20:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << add_zz_z(a, b);
		break;
	case 4:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (add_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << add_nn_n(a, b);
		break;
	case 6:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (mul_nd_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mul_nd_n(a, b);
		break;
	case 22:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mul_zz_z(a, b);
		break;
	case 21:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << sub_zz_z(a, b);
		break;
	case 2:
		cout << "Input your number: ";
		cin >> a;
		if (nzer_n_b(a))
		cout << "A = 0";
		else
		cout << "A != 0";
		break;
	case 3:
		cout << "Input your number: ";
		cin >> a;
		if (add_1n_n(a) == -1 || a != int(a))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << add_1n_n(a);
		break;
	case 5:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (sub_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << sub_nn_n(a, b);
		break;
	case 8:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (mul_nn_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mul_nn_n(a, b);
		break;
	case 9:
		cout << "Input your numbers: ";
		cin >> a >> b >> c;
		if (sub_ndn_n(a, b, c) == -1 || a != int(a) || b != int(b) || c != int(c))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << sub_ndn_n(a, b, c);
		break;
	case 7:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (mul_nk_n(a, b) == -1 || a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mul_nk_n(a, b);
		break;
	case 1:
		cout << "Input your numbers: ";
		cin >> a >> b;
		if (a != int(a) || b != int(b))
			cout << "Invalid input numbers" << endl;
		else
		switch (com_nn_d(a, b))
		{
			case -1:
				cout << "Invalid input numbers" << endl;
				break;
			case 0:
				cout << "A = B" << endl;
				break;
			case 2:
				cout << "A > B" << endl;
				break;
			case 1:
				cout << "A < B" << endl;
				break;
		}
		break;
	case 17:
	{
		cout << "Input your number: ";
		cin >> a;
		if (a != int(a))
			cout << "Invalid input numbers" << endl;
		else
			cout << "Answer: " << mul_zm_z(a);
		break;
	}
	default:
		cout << "Invalid choice" << endl;
	}
	cout << endl;
	system("pause");
}
