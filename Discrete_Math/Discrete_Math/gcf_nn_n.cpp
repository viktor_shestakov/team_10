// GCF_NN_N.cpp 
// ��������: ���������� ����, ������ 9375
// ��� ����������� �����
#pragma once
#include "mod_nn_n.h"
#include "nzer_n_b.h"

int gcf_nn_n(int a, int b)
{
	if (a < 1 || b < 1)
		return -1;
	if (a < b)
	{
		int f = a;
		a = b;
		b = f;
	}
	int c;
	do {
		c = mod_nn_n(a, b); // a % b
		a = b;
		b = c;
	} while (nzer_n_b(c));
	return a;
}