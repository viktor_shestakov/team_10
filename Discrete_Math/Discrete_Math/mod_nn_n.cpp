// MOD_NN_N.cpp 
// ��������: ���������� ����, ������ 9375
// ������� �� ������� �������� ������������ ����� �� ������� ��� ������ ����������� � ��������(�������� ������� �� ����)
#pragma once
#include "div_nn_n.h"
#include "com_nn_d.h"

int mod_nn_n(int a, int b)
{
	int c = -1;
	int d;
	if (a < 1 || b < 1)
		return c;
	c = div_nn_n(a, b); // ������� �� ������� 
	if (com_nn_d(a, b) == 2) // ���� a > b
		d = a - b * c; // ������� �� ������� 
	else
		d = b - a * c;
	return d;
}