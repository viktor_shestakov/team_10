// ��������� ������������ ����� �� 10 ^ k
#pragma once

int mul_nk_n(long int a, long int k)
{
	int c = 1;
	if (k < 0 || a < 1)
		return -1;
	for (int i=0; i<k; ++i)
		c *= 10;
	return a * c;
}
