
// DIV_NN_N
// ��������: ���������� ����, ������ 9375
//������� �� ������� �������� ������������ ����� �� ������� ��� ������ ����������� � �������� (�������� ������� �� ����)
#pragma once
#include "com_nn_d.h"

int div_nn_n(int a, int b)
{
	int c = -1;
	if (a < 1 || b < 1)
		return c;
	if (com_nn_d(a, b) == 2) // ���� � > b
		c = a / b;
	else
		c = b / a;
	return c;
}